/*
From a set of permutations for a given number n find 
the number of elements such hat no element appears in its original position.
*/

//ststus :- done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}


ll permute(int n )
{
	ll a = 0 ;
	ll b = 1 ;
	for(int i = 3 ; i <= n ; i++) 
	{
		ll c = (i-1) * ( b + a) ; 
		a = b ;
		b = c ; 
	}
	return b  ; 
}


int main()
{
	sj73();
    
    int n ; 
    cout << "Enter the number :";
    cin >> n ; 
   
    ll res =permute(n); 
    cout << "The required result : " ; 
    cout << res ; 

	return 0 ; 
}