/*Write a program to find partial sums of the terms of the Fibonacci 
sequence.*/

// status = done  


// partial sum = diffrence between two terms of a fibonacci series 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

ll fibbonacci(int n )
{ 
	if (n <= 1)
		return n ; 
	return fibbonacci(n-1) + fibbonacci(n-2) ; 
}

int main()
{  
	sj73();
	int n , m; 
	cout << "Enter the index of 2 numbers in the series  : " ;
	cin >> n >> m; 
    ll res1 = fibbonacci(n);
    ll res2 = fibbonacci(m); 
	cout << "\nThe required result is : ";
	cout << abs (res2 - res1) ; 
	
	return 0 ; 
}