// Given two long integers numbers min and max find and print a 
// typical part n/d to such an extent that min<=d<=max and |n/d-pi|
// is negligible if there are a few divisions havingan insignificant
// distance to pi pick the one with the littlest denominator.


//Status = Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();

    int min , max ; 
    cin >> min >> max ; 
    ld pi = 3.141592653589 ;
    int d ,n ;
    int f_d , f_n ; 
    ld frac = 0.0; 
    ld min_diff = 1 ;

    for(d = min  ;  d <= max ; d++)
    {
        float nn = pi*d ;
        if((int(nn*100) )%100 >=50)
            n = nn +1;

        else n = nn ;
        frac = n/float(d) ; 
        ld temp = abs(frac-pi) ;
        if(min_diff > temp )
        {
            min_diff = temp ;
            f_d = d ; 
            f_n = n ;
        }   
       
    }
    cout <<f_n<<"/"<<f_d;


	return 0 ; 
}