//Program to check if two rectangles, not necessarily aligned with the X and Y axes, intersect.
 
//Status = Done 



// One rectangle is above top edge of other rectangle
// One rectangle is on left side of left edge of other

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

struct Point
{
	int x , y ; 

} ;

bool check(Point p1 ,Point q1 ,Point p2 , Point q2 )
{
	if (p1.x == q1.x || p1.y == q1.y || p2.x == q2.x || p2.y == q2.y) 
        return false;
 
    if (p1.y >= q2.y || p2.y >= q1.y)
        return false;

    if (p1.x >= q2.x || p2.x >= q1.x)
        return false;

    return true;

}


int main()
{
	sj73();
    
    Point p1, q1 , p2 , q2; 
    //first rectangle cordintes ;
	cin >> p1.x >> p1.y ;
	cin >> q1.x >> q1.y ;
	//second rect. cordinates 
	cin >> p2.x >> p2.y ;
	cin >> q2.x >> q2.y ;


	if(check(p1,q1,p2,q2) == true )
		cout <<"-----Rectangles do overlap----- " ;
	else 
		cout <<"-----Rectangles donot overlap----";



	return 0 ; 
}