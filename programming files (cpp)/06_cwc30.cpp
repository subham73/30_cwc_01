// Program to swap bits with O(1) time complexity.


//Status : Done 

//doubt 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int swapBits( int num, int p1, int p2, int n)
{

	int Xor = ((num >> p1) ^ (num >> p2)) & ((1U << n) - 1);
    return num ^ ( (Xor << p1) | (Xor << p2));
}

int main()
{
	sj73();
    
    int num , p1 ,  p2  , n ; 
    cin>> num >> p1 >> p2 >> n ;
  	int res = swapBits( num , p1 , p2 , n);
    cout << "Result = " << res;

	return 0 ; 
}


