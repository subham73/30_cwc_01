// Find the sum of the digits in the number 100!


// status done 





#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;
int const Nmax = 1000;
void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
    
	int res[Nmax] , carry = 0 ;
	res[0] = 1;
	int size = 1 ;
	int prod ;
	int n ;
	cin>>n ; 						//100
	for(int i = 2 ; i <= n ; i++ ) // for finding value which will be multiplied 
	{
		for(int j = 0 ; j < size ; j++) // for rotating and multiplyig
		{
			prod = res[j]*i +carry ;
			res [j] = prod % 10 ; // backside 
			carry = prod / 10 ;
		}
		while(carry>0)
		{
			res[size] = carry %10 ; 
			carry = carry/10 ;
			size++;
		}
	}
	ll sum = 0 ;
	for(int i = 0 ; i <size ; i++)
	{
		sum+=res[i] ;
	}

	cout <<"Sum of digits of 100! is: " <<sum ;

	return 0 ; 
}