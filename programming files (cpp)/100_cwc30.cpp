// Find the sum of all numbers, less
// than one million, which are palindromic in base 10 and base 2.


//status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}


string base_10(int n)
{
    string str;
    while (n > 0) {
        int digit = n % 2;
        n /= 2;
        str.push_back(digit + '0');
    }
    return str;
}
 

int isPalindrome(int i)
{
    int temp = i;
     
    int m = 0;
    while (temp > 0) 
    {
        m = temp % 10 + m * 10;
        temp /= 10;
    }
     
    if (m == i) 
    {  
        string str = base_10(m);
        string str1 = str;
        reverse(str.begin(), str.end());
        if (str == str1) 
            return i;
    }
    return 0;
}

ll sum(int n)
{     
    int sum = 0;
    for (int i = 1; i <= n; i++) 
    {
        sum += isPalindrome(i);
    }
   	return sum ; 
}

int main()
{
	sj73();
	int n = 1000000; 
	ll res = sum(n);
	cout<<"Total sum :" << res ;
	return 0 ; 
}

