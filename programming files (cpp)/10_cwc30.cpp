// Write a program that has three numbers a, b, c where a < b < c and a^2 + b^2 = c^2. There exists only one solution where 
// it satisfies the following equation for a+ b + c = 1000. Find the product abc



//Status : done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}



ll solution(int n)
{
    ll ans = 0;
    for (int a = 1; a <= n; a++) 
    {
        for (int b = a;  b <= n; b++) 
        {
            int x = a * a + b * b;
            int c = sqrt(x);
            if (c * c == x && c <= n && a+b+c == 1000)
				ans = a*b*c ;
        }
    }
 
    return ans;
}

int main()
{
	sj73();
    
	ll res = solution(1000);

	cout <<"Product is : " <<res ; 

	return 0 ; 
}



