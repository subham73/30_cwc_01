// Xonas is residing in Albert’s House and he wants to go outside such that he can reach 
// the pizza store in minimum time. You have to find the shortest distance between the store
 // and Albert’s House so that Xonas have to do least effort for a pizza. Xonas is very good
 // in mathematics and he anyhow manages to find the X and Y coordinates of the destination 
 // and initial position.


// status : solved 



#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
 #define ps(x,y)	fixed<<setprecision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

struct point 
{
	int x1 ,  y1 ;
	int x2 ,  y2 ;
} ;


int main()
{
	sj73();
    int n ;
	cout << "Enter the total no. of paths you want to compare for : ";
	cin >> n  ;
	point P[n];
	float a[n] ;
	float min_d = INT_MAX ; int path ;
	for(int i = 0 ; i < n ;i++)
	{
		string ch ; 
		if ((i+1)%10 == 1 )
			ch = "st";
		else if  ((i+1)%10 == 2 )
			ch = "nd" ;
		else if ((i+1)%10 == 3)
			ch = "rd" ;
		else 
			ch = "nd";
 
		cout << "Enter the constraints for " << (i+1) << ch << " path:" << endl;
		cout << "Enter X and Y coordinates of Initial position: ";
		cin >> P[i].x1 >> P[i].y1 ; 
		cout << "Enter X and Y coordinates of Final position: ";
		cin >> P[i].x2 >> P[i].y2 ;

		// input end ;
		float distance = sqrt( pow( ( P[i].y2-P[i].y1 ) , 2 ) + pow( ( P[i].x2-P[i].x1 ) , 2) ) ; 
		a[i] = distance ;
		if( min_d > distance )
		{
			min_d = distance ; 
			path = i+1;
		}
			
	}
	for ( int i = 0 ; i < n ; i++ )
		cout << ps (a[i] , 2)<<" Units Distance will be covered in path "<<(i+1)<<endl ;
	
	cout <<endl<<"You should go with path "<<path <<"." ;

	return 0 ; 
}