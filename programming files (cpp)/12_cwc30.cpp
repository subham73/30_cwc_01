// Write a Program that will convert an integer to to it’s corresponding 2’s Complement in
// binary representation.
 



// status : done 


//2's compliment 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}




int main()
{
	sj73();

	int n ;
    cout << " Enter the Integer no.: ";
    cin >> n ; 

    vector<int> v ;
    while(n>0)
    {
        int temp = n%2 ;
        v.push_back(temp);
        n/=2;
    }
    int zero = v.size()%4 ;
    for(int i = 0 ; i < 4-zero ;i++)
        v.push_back(0) ;

    for(int i = 0 ; i <v.size() ; i++)
    {
        if(v[i] == 0 )
            v[i] = 1;
        else v[i] = 0 ;
    }
    if(v[0]==0)
        v[0] =1 ;
    int id = 1; 
    for(int i = 1 ; v[i]!=0 ; i++)
    {
         v[i]=1;
         id = i ;
    }   
    v[id] = 1;
    

    cout <<" 2 ’s complement for the no. is ";
    for(int i = v.size()-1 ; i >= 0 ; i--)
        cout<<v[i];
    cout<<" in binary representation.";

	return 0 ; 
}