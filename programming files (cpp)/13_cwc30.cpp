// A member of CWC asks a random question from different coordinators of the club. 
// The member of the club gets an answer from xyz kumar. Write a code that will keep
 // track of question asked by and answer given by in the corresponding index.



//status : done


//allignment question (cpp main hota nehi hai :(


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

struct name
{
	string s, r ;
} ;

int main()
{
	sj73();
    
    int n; 
	cout << "Enter the total no. of questions asked: " ;
	cin >> n ; 
	name n_[n];
	for(int i = 0 ; i < n ; i++)
	{
		string ch ; 
		if ((i+1)%10 == 1 )
			ch = "st";
		else if  ((i+1)%10 == 2 )
			ch = "nd" ;
		else if ((i+1)%10 == 3)
			ch = "rd" ;
		else 
			ch = "nd";

		cout << "Enter asker’s and coordinator’s name for "<<i+1<<ch<<" one:";
		cin >> n_[i].s >> n_[i].r ;
	}

	string space ="         ";
	cout << "--------------------------------------------------------------------\n";
	cout.width(14);
	cout << "Asker";
	cout.width(37);
	cout << "Query Solved by "<<endl;
	
	cout << "         ---------             --------------------------"<<endl;
	
	for(int i = 0 ; i < n ; i++)
	{
		cout << space;
		cout << n_[i].s<<setw(30 - n_[i].s.length()+n_[i].r.length());
		cout << n_[i].r;
		cout << endl;
	}
	cout << "--------------------------------------------------------------------\n";

	return 0 ; 
}