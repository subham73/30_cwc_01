// Write a program to print the series : 1, 2, 3, 4, 5,6 ,7, 8 ,9 ,10 , 12 , 21, 23,32
// ,34, 43, ……. The following series will depend upon upto how many numbers user 
// want to print the series


// status : done 




#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}


int main()
{
	sj73();
	int n ;
	cout <<"Enter the range of numbers :" ;
	cin>> n ;
	int a[n] ;  

	if(n <= 10)
	{
		for (int i = 1 ; i < n ; i++)
			cout <<i <<" ,"  ;
		cout << n ; 
	}
	else
	{
		int term = 10;
		for (int i = 0 ; i < 10 ; i++)
			a[i] = i+1;
		for(int i = 1 ; i <= n-10 ; i++)
		{
			if(i%2 != 0)
				term += 2 ; 
			else 
			{
				term = (term%10)*10+(term/10);
				
			}
			a[9+i] = term ; 
		}
		
		cout<<"Series Generated: ";
		for(int i = 0 ; i < n-1 ; i++)
			cout<<a[i]<<" ,";
		cout<<a[n-1];
	}

	return 0 ; 
}