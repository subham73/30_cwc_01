//Given a pole of length n and a rundown of costs of poles of length I
// , where 1 <= I <= n, track down the ideal method to cut the pole into more modest poles
 // to augment benefit.



//Status :done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}
int length[] = {1, 2, 3, 4, 5, 6, 7, 8};
int price [] = {1, 5, 8, 9, 10, 17, 17, 20};

int max_c(int n )
{

	// base 
	if (n == 0 )
		return 0  ;

	int max_cost = INT_MIN ; 

	for(int i = 0 ; i < n ; i++)
		max_cost = max(max_cost , price[i]+max_c(n-i-1));
	
	return max_cost ; 
}


int main()
{
	sj73();
    

	int n ; cin >> n ;
	int res = max_c(n) ; 
	cout<< res ; 
	
	return 0 ; 
}