// 10 persons are working on a Machine Learning model and they want to make a program 
// which will find the array’s least complexity for different elements for processing 
// that array to be used in the ML model.The program will display the no. of operations , 
// Cost/ scalar multiplications for each of the sequences.



//Status : Done 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;



void sj73()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    #ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int MatrixChainOrder(int p[], int i, int j)
{
    if (i == j)
        return 0;
    int k;
    int min = INT_MAX;
    int count;
 
    for (k = i; k < j; k++)
    {
        count = MatrixChainOrder(p, i, k)
                + MatrixChainOrder(p, k + 1, j)
                + p[i - 1] * p[k] * p[j];
 
        if (count < min)
            min = count;
    }

    return min;
}


int min_val(int res[] , int t )
{
    int min = INT_MAX ;
    int pos ; 
    for(int i = 0 ; i < t ; i++)
    {
        if(min>res[i])
        {
            min= res[i];
            pos = i;
        }
    }
    return pos+1;
}

int main()
{
    sj73();
    int n ,t;
    cout<<"How many numbers you want to store for the matrix chain multiplication? ";
    cin >> n ; 
    cout<<"How many sequences you want to check to find the optimised one? ";
    cin >> t;
    int res[t] , query [n]; 
    for(int i = 0 ; i < t ; i++)
    {
        cout<<"\n For Sequence No." <<i+1;
        cout<<"\n-----------------------------------------\n";
        for(int j = 0 ; j < n ; j++)
        {
            cout<<"Enter the elements ";
            cin>>query[j];
        }
        res[i] = MatrixChainOrder(query , 1 , n-1) ;
        cout<<"-----------------------------------------";
    }
    int pos = min_val(res , t ); 

    cout<<"\nAll the sequences with their operations are [";
    for(int i = 0 ; i < t ; i++)
        cout<<res[i]<<",";
    cout << "] and among them. \n" ;
    cout << "Best sequence for the ML model that runs in least time will be the sequence no : "
                    << pos <<"and the no of operations will be "<< res[pos-1] ;


    return 0 ; 
}

