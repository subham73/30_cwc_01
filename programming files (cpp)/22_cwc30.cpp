//Multiply two numbers with using only Bitwise Operators.

//Status : Done 



#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)    fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    #ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int multi(int a,  int b)
{
    int res = 0; 
    while (b > 0)
    {
                
        if (b & 1)
            res = res + a;
 
        a = a << 1;
        b = b >> 1;
    }
    return res;
}


int main()
{
    sj73();
    int a , b ;
    cin>> a>> b;
    cout << multi(a,b) ;


    return 0 ; 
}




