// Given an unsorted array arr[] which contains both positive and 
// negative numbers.Your task is to find the first positive missing numbers.


//Status : Done 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}
int const Nmax = 10000 ; 
int fr[Nmax];

int main()
{
	sj73();

	int n ; 
	cin >> n ; 

	int a[n] ;
	for(int i = 0 ; i < n ; i++)
		cin>>a[i];

	for(int i = 0 ; i < n ; i++)
	{
		if(a[i] > 0)
			fr[a[i]] = 1 ;
	}
    
    for(int i = 1 ; i <Nmax ; i++)
    {
    	if(fr[i] != 1)
    	{
    		cout<<i ;
    		return 0 ;
    	}
    }

	return 0 ; 
}