// Write a java program to accept numbers in a matrix of 
// size 4 x 4 by using Scanner class then calculate and display sum of bordered elements.



//Status : Done 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
    
	int n = 4 ;
	int a[n][n]  ;

	for(int i = 0 ; i < n ; i++)
		for(int j = 0 ; j < n ; j++)
			cin>>a[i][j] ;

	int sum = 0 ;
	for(int i = 0 ; i < n ; i ++)
	{
		for(int j = 0 ; j < n ; j++)
		{	
			if( i == 0 || i == n-1)
				sum += a[i][j] ; 
			else 
			{
				sum += a[i][0] + a[i][n-1];
				break ; 
			}

		}
	}

	cout <<"Sum = "<<sum ; 





	return 0 ; 
}
