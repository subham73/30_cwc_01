// Binomial co-efficient can be calculated by using the following formula : 
// nCm = n!/m!(n–m)! (where ! sign represents the factorial of a number)
 // WAP in java to calculate and print the binomial co-efficient of the given expression,
  // taking the value n and m as input . Make use of the function int fact( int k ), 
  // which returns the factorial of a number k.

//Status : solved


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int fact(int n)
{
    int res = 1;
    for (int i = 2; i <= n; i++)
        res = res * i;
    return res;
}

int binomial (int n, int r)
{
    return fact(n) / (fact(r) * fact(n - r));
}
 


int main()
{
	sj73();
    
 	int n , r ;
 	cin >> n >> r ;
    cout << binomial (n, r);
    return 0; 
}