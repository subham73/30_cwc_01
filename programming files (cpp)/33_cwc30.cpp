// Seema is a teacher.She has got the job to arrange N number of papers 
// in ascending order by there marks. There are T number of sections. 
// Help her do the work.


//Status : Solved 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
	int t ; 
	cin >> t ;
	int n ; 
	while(t -- )
	{
		cin >> n ;  
		int a[n] ; 
		for(int i = 0 ; i < n ; i++)
			cin>>a[i] ; 
		sort(a,a+n) ;
		for(int i = 0 ; i < n ; i++)
			cout<<a[i] <<" " ;
		cout<<endl ; 

	} 

  


	return 0 ; 
}
