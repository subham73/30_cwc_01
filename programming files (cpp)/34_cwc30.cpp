// You are given two integer arrays a and b of
 // length n. You can reverse at most one subarray
  // (continuous subsegment) of the array a. Your task is to
   // reverse such a subarray that the sum ∑i=1nai⋅bi is maximized.


//Status  : Done  
// doubt

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}


const int Nmax=5005;
int a [Nmax],b [Nmax],c [Nmax][Nmax];
int main()
{
	sj73();

	int n;
	cin>>n;
	for(int i = 1 ; i*i <= n*n ; i ++) 
		cin >> a[i];

	int init = 0 , maxx=0;

	for(int i = 1;i <= n;i ++) 
	{
		cin >> b[i];
		init += b[i] * a[i];
	}

	for(int l = n ; l >= 1;l --)
	{
    	for(int r = l+1 ; r <= n ;r  ++)
     	{
        		c [l][r] = c [l+1][r-1] - a[l]*b[l] - a[r]*b[r] + a[l]*b[r] + a[r]*b[l];
      	 		maxx = max (maxx,c [l][r]);
      	}
    }
	cout << init+maxx;
 	
 	return 0 ; 
} 
