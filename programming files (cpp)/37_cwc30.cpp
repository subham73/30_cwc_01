
// The Gray code is a well-known concept. One of its important 
// properties is that every two adjacent numbers have exactly one different
//  digit in their binary representation. In this problem, we will give you
//   n non-negative integers in a sequence A[1..n] (0<=A[i]<2^64), such
//    that every two adjacent integers have exactly one different digit
//     in their binary representation, similar to the Gray code. Your
//      task is to check whether there exist 4 numbers A[i1], A[i2], 
//      A[i3], A[i4] (1 <= i1 < i2 < i3 < i4 <= n) out of the given n
//       numbers such that A[i1] xor A[i2] xor A[i3] xor A[i4] = 0.




//Status : done









#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
  	int n;
  	cin >> n;
	unsigned long long int a[n];
	for(int i=0; i<n; i++)
		cin >> a[i];
	  
	if(n>=130)
	{
		cout << "Yes" << endl;
		return 0;
	}
	  
	for(int i=0; i<n; i++)
		for(int j=i+1; j<n; j++)
		  	for(int k =j+1; k<n; k++)
				for(int l=k+1; l<n; l++)
			  		if((a[i]^a[j]^a[k]^a[l])==0)
			  		{
						cout << "Yes" << endl;
						return 0;
			  		}

  cout << "No" << endl;



	return 0 ; 
}