//You are given a multiset of N integers. Please find such a nonempty subset of it that the 
//sum of the subset's elements is divisible by N. Otherwise, state that this subset doesn't 
//exist.

//status : done



#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;
#define INF 1e18
// #define mod 1e9+7  //	
// #define fin for(int i=0;i<n;i++)	
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)
void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
  
    int t;
    cin>>t;
    while(t--)
    {
        int n;
        cin>>n;
        int a[n];
        ll sum=0;
        vector<vector<int>> v(n);
        v[0].push_back(-1);

        for(int i=0;i<n;i++)
        {
            cin>>a[i];
            sum+=a[i];
            v[sum%n].push_back(i);
        }
        
        for(int i=0;i<n;i++)
        {
            if(v[i].size()>1)
            {
                int d=v[i][0];
                int b=v[i][1];
                cout<<abs(b-d)<<endl;
                for(int j=d+1;j<=b;j++)
                    cout<<j+1<<" ";

                break;
            }
        }

    }
        
}

