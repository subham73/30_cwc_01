// Peter wants to generate some prime numbers for his cryptosystem. Help him! Your task is to generate 
// all prime numbers between two given numbers!



//Status solved 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}


const int Nmax = 100001; 
bool isPrime[Nmax]; 
void prime(int limit) 
{ 
	for(int i = 2 ; i <= limit ; i++) 
		isPrime[i] = true; 
	for(int i = 2 ; i <= limit/2 ; i++) 
	{ 
 		if(isPrime[i]==true) 
		{ 
			for(int j = i*2 ; j <= limit ;j += i ) 
				isPrime[j] = false ; 
		} 
	}  
} 
int main()
{
	sj73();
	int t ; 
	int n , m ; 
	cin >> t ; 

	while(t--)
	{
		cin >> n >> m ; 
		prime(max(n , m )) ;
		for(int i = n ; i <= m ; i++)
		{
			if(isPrime[i] == true )
				cout << i <<endl ; 
		}
		cout<<endl ; 
	}
    






	return 0 ; 
}