// You are given a sequence of numbers a1, a2, ..., an, and a number m. Check if it is possible to choose a non-empty subsequence aij such
// that the sum of numbers in this subsequence is divisible by m.


//status : Solved 

//bitset question 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

const int N = 1e3 ; 
bitset<N> b ={true};

int main()
{
	sj73();
    
    int n , m ; 
	cin >> n >> m;
	for (int i = 0; i < n; i++) 
	{
		int x;
		cin >> x;
		x = (x % m ? x % m: m);  //check 
		b |= b << x, b |= b >> m; // using  bitset 

	}
	cout << (b[m]? "YES": "NO");

	return 0 ; 
}

