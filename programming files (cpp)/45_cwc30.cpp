// WAP to Minimize given integer by swapping pairs of unequal
 // bits(0,1) in its binary representation.


// status : done 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}


int toBinary(int n)
{
	vector<int> a;
	int num =0 ;
	int count0 =0;
	while(n>0)
	{
		int temp = n%2 ;
		a.push_back(temp); 
		n /= 2 ;
	}

	for(int i = 0 ; i < a.size() ; i++)
	{
		if(a[i] == 0 )
			count0 ++;
	}

	for(int i = 0 ; i < a.size()-count0 ; i++)
	{
		a[i] = 1 ;
	}

	for(int i = a.size()-count0 ; i < a.size() ; i++)
	{	
		a[i] = 0 ;
	}

	for(int i = 0 ; i < a.size() ; i++)
	{
		num += a[a.size()-i-1]*pow(2,i);
	}
	return num ; 
}

int main()
{
	sj73();
	int  n ;
	cin >> n ; 

	cout << toBinary(n); 

   

	return 0 ; 
}