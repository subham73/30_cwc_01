//WAP to find roots of any quadratic equation.


//Status : solved 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();

    double a, b, c;
    cin>>a>>b>>c;
	
	double  disc, r1, r2, rl, ig;
    disc = b * b - 4 * a * c;


    if (disc > 0) 
    {
        r1 = (-b + sqrt(disc)) / (2 * a);
        r2 = (-b - sqrt(disc)) / (2 * a);
        cout<<"r1 ="<<r1 <<"   r2 =" << r2;
    }

    else if (disc == 0) 
    {
        r1 = r2 = -b / (2 * a);
        cout<<"r1 = r2 = " <<r1;
    }

    else 
    {
        rl = -b / (2 * a);
        ig = sqrt(-disc) / (2 * a);
        cout<<"r1 ="<<rl <<"+i"<<ig<<endl;
        cout<<"r2 ="<<rl <<"-i"<<ig<<endl;
    }

	return 0 ; 
}