// Write a program to find the maximum number of distinct prime factors a 
// number has in a given range and print the max number of factors.

//Status : solved 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int solve(int m, int n)
{
    bool prime[n + 1];
	ll count[n + 1];
 
    for (int i = 0; i <= n; i++) 
    {
        count[i] = 0;
        prime[i] = true;
    }
 
    for (int i = 2; i <= n; i++) 
    {

        if (prime[i] == true) 
        {
            count[i] = 1;
            for (int j = i * 2; j <= n; j += i) 
            {
                count[j]++;
                prime[j] = false;
            }
        }
    }

    int max = count[m];
    int num = m;

    for (int i = m; i <= n; i++) 
    {
 
        if (count[i] > max) 
        {
            max = count[i];
            num = i;
        }
    }
    return num;
}

int main()
{
	sj73();
	int n , m ; 

	cin >> n >> m ; 
	cout <<solve(n,m) ;

	return 0 ; 
}