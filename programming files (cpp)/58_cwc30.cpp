1// roman to integer 

// Ststus : solved 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int roman_to_int(string inp)
{
    map<char,int> m;
    m['I'] =   1;
    m['V'] =   5;
    m['X'] =  10;
    m['L'] =  50;
    m['C'] = 100;
    m['D'] = 500;
    m['M'] =1000;
    int op=0,i=0;
    

    if(inp.length()<=1)
        return m[inp.at(0)];
  
    else
    {
        while(i<inp.size())
        {
            if(m[inp[i]]<m[inp[i+1]])
            {
                op+=m[inp[i+1]]-m[inp[i]];
                i+=2;
            }
            else
            {
                op+=m[inp[i]];
                i++;
            }
        }
        return op;
    }
}

int main()
{
	sj73();
    string inp;
    getline(cin,inp);
    
    int op;
    op=roman_to_int(inp);
    
    cout<<op;
    

	return 0 ; 
}