// Write a program to create a array of size N. Then perform a number of right circular
// rotations and return the values of the elements at the given indices.


//Status : Done 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}
void rotate(int a[], int n)
{
    int t = a[n-1] , i;
    for (i = n-1 ; i > 0 ; i--)
    a[i] = a[i - 1];
    a[0] = t;
}

int main()
{
	sj73();
	int n ; 
	cin >> n ; 
	int rot ; 
	cin>> rot ;
	int a[n] ; 
	for(int i = 0 ; i < n  ; i ++)
		cin>>a[i];

	while( rot --)
		rotate(a,n);

	for(int i = 0 ; i < n  ; i ++)
		cout<<a[i]<<" ";




	return 0 ; 
}