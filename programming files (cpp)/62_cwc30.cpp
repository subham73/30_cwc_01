// Write a program to take N numbers as input. Then display only prime 
// - palindromes using method overloading concept.


//Status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int digits(int n)
{

    return (n >= 0 && n < 10);
}
 

bool isPalUtil(int n, int* Dm)
{

    if (digits(n))
        return (n == (*Dm) % 10);

    if (!isPalUtil(n/10, Dm))
        return false;

    *Dm /= 10;

    return (n % 10 == (*Dm) % 10);
}

int check_palin(int n)
{
    if (n < 0)
       n = -n;
    int *Dm = new int(n);
    return isPalUtil(n, Dm);
}

void palin_prime(int n)
{

    bool prime[n+1];
    memset(prime, true, sizeof(prime));
 
    for (int i=2; i*i<=n; i++)
    {
        if (prime[i] == true)
        {
 
            for (int i=i*2; i<=n; i += i)
                prime[i] = false;
        }
    }
    for (int i=2; i<=n; i++)
       if (prime[i] && check_palin(i))
          cout << i << " ";
}

int main()
{
	sj73();
    
    int n ; 
    cin >> n ;
    palin_prime(n) ; 


	return 0 ; 
}