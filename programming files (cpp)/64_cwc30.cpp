// William planned to choose a four digit lucky number for his car.
// His lucky numbers are 3,5 and 7. Help him find the number,
// whose sum is divisible by 3 or 5 or 7. Provide a valid car number,
// Fails to provide a valid input then display that number is not a valid car
// number.

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;



void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int term(int n )
{
	int count = 0 ;
	while(n>0)
	{
		n/=10;
		count++;
	}
	return count ;

}
int Sum (int n )
{
	int sum  = 0 ;
	while(n>0)
	{
		sum += n % 10;
		n /= 10;
	}
	return sum ;
}

int main()
{
	sj73();
	int n , m  ; 
	cin >> m ;
	n = abs(m) ; 
	if(term(n) != 4 )
	{
		cout<<m<<" is not a valid car number";
		return 0 ;
	}

	int sum = Sum (n) ;
	int flag = 0 ;
	if(sum % 3 == 0 || sum % 5 == 0 || sum % 7 == 0  )
		flag = 1 ;
	if(flag == 1)
		cout <<" Lucky Number";
	else
		cout<<"Sorry its not my lucky number";

	return 0 ; 
}