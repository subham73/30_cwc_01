// The number will be unique if it is positive integer 
// and there are no repeated digits in the number.
// In other words, a number is said to be unique if and 
// only if the digits are not duplicate. For example, 20, 
// 56, 9863, 145, etc. are the unique numbers while 33, 121, 
// 900, 1010, etc. are not unique numbers


//Satus : done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

bool unk(int n )
{
	int fr[10];
	for(int i = 0 ; i < 10 ; i++)
		fr[i] = 0;
	while(n>0)
	{
		int rem = n%10 ;
		fr[rem]++;
		n/=10 ;
	}
	for(int i = 0; i < 10 ; i++)
	{
		if(fr[i]>1)
			return false ;
	}
	return true ;
}

int main()
{
	sj73();
	int n ; 
	cin >> n ;

	if(unk(n) == true )
		cout<<"The number is Unique";
	else 
		cout<<"The number is NOT Unique" ;    


	return 0 ; 
}