// WAP that given array and split it from a specified 
// position, and move the first part of the array add to the end.

//Status = Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
	int n ;
	cin >> n ; 
	int k ; 
	cin>> k ; 
	int a[n] ;   
	for(int i = 0 ; i < n ; i ++)
    	cin>>a[i] ;
    int temp[k] ; 
    
    for(int i = 0 ; i < k ; i++ )
    {
    	temp[i] = a[i];
    }
    //moving forward 
    for(int i = 0; i < n-k  ; i++ )
    	a[i] = a[k+i] ;
    for(int i = 0 ; i < k ; i++ )
    	a[n-k+i] = temp[i];

    for(int i = 0 ; i < n ; i++ )
    	cout<<a[i]<<" "; 
    

	return 0 ; 
}