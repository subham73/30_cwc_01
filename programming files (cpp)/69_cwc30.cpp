//WAP that given a number n, the task is to find the even factor sum of a number.


//Status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

ll divideBy(int n )
{
	ll sum = 0; 
	for(int i = 2 ; i <= n ; i += 2)
	{
		if( n%i == 0 )
			sum += i ;
	}
	return sum ;
}

int main()
{
	sj73();
    
    int n ; 
    cin >> n ; 
 
	ll sum = divideBy(n);

	cout <<sum ; 



	return 0 ; 
}