// sin(x)

//Status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
#define ps(x,y)	fixed<<setprecision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();

	float x ;
 	cin >>x ; 
    ld term=1,sum =0;
    int i =0,sign =1;
    while(term >0.000001)
    {   
        int n = 2*i+1;
        ld fact=1;
        for(int j =2;j<=n;j++ )
        {
            fact*=j;
        }
        term = pow(x,n)/fact;
        sum += sign *term;
        sign *=-1;
        i++;
    }
 	cout <<ps(sum,2) ;

	return 0 ; 
}