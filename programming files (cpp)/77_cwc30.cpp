// Given an array a[] of size n and a number num ,write a program 
// to find if there exists a pair of elements in the array 
// whose difference is num.


//status : Done 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();

	int n ; 
	cin >> n ; 
 
 	int num ; 
	cin >> num ;

	int a[n] ;
	for(int i = 0 ; i < n ; i++)
		cin >> a[i] ;
    
    int count = 0 ; 
    for(int i = 0 ; i < n-1 ; i++ )
    {
    	for(int j = i+1 ; i < n ; j++)
    	{
    		 if( abs (a[i]-a[j]) == num )
    		 	count++;
    	}
    }

    cout << count ; 

	return 0 ; 
}