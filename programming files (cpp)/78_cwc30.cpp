// Given an array a[ ] of 
// size n, swap the p th element from beginning with p th element from end.



//Status : solved

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
    
	int n ; 
	cin >> n ; 

	int p ; 
	cin>> p ; 

	int a[n] ; 
	for(int i = 0 ; i < n ; i++ )
		cin>>a[i] ;

	int temp = a[p-1] ;
	a[p-1] = a[n-p] ; 
	a[n-p] = temp ; 

	for(int i = 0 ; i < n ; i++)
		cout << a[i] <<" " ; 


	return 0 ; 
}