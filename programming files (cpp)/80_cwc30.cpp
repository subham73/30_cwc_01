// Given two numbers m and n , you are required to check 
// whether they are anagram of each other or not in binary 
// representation?


//Status :done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int find_b(int n )
{
	int count = 0 ; 
	while(n>0)
	{
		if(n%2 == 1)
			count++ ;
		n/=2;
	}
	return count ;
}

int main()
{
	sj73();

	int n , m ; 
	cin >> n ;
	cin >> m ;
	int n_ = find_b(n) ;
    int m_ = find_b(m) ;

    if(n_ == m_)
    	cout<<" True";
    else 
    	cout<<" False";

	return 0 ; 
}