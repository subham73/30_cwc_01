// Given a sorted array containing only 0s and 1s, find the 
// transition point. Note: return -1 if there is no transition point.

//status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
	int n ; 
	cin >> n ; 
	int pos = -1;
	int a[n] ; 
	for(int i = 0 ; i <n ; i++)
		cin>> a[i] ; 
	 
	for(int i = 0 ; i < n-1 ; i++)
	{
		if(a[i+1]-a[i] == 1 )
		{
			pos = i+1 ;
			break ;
		}
	}    
	cout << pos ; 





	return 0 ; 
}