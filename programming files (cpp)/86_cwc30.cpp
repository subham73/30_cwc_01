// Write a program to generate 
// and show all Kaprekar numbers less than 1000.

//Statsu : done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

 
void kaprekar(int n , int &count )
{
    if (n == 1)
    {
    	cout<<setw(8)<<left <<n <<setw(8)<< n <<setw(10)<<"0 + 1";
    	cout<<endl;
    	return  ;
    }
 	

    int sqr_n = n * n;
    int ctr_digits = 0;
    while (sqr_n)
    {
        ctr_digits++;
        sqr_n /= 10;
    }
 
    sqr_n = n*n; 
 
    for (int r_digits=1; r_digits<ctr_digits; r_digits++)
    {
        int eq_parts = pow(10, r_digits);

        if (eq_parts == n)
            continue;
 
        int sum = sqr_n/eq_parts + sqr_n % eq_parts;
        if (sum == n)
           {
           		count += 1;
           		cout<< setw(8)<<left<< n <<setw(8) << sqr_n ;
           		cout<< sqr_n/eq_parts <<" + "<< sqr_n % eq_parts;
           		cout<< endl;
           }
    }
}

int main()
{
	sj73();
    int count = 1 ;
  	for (int i=1; i<1000; i++)
    	kaprekar(i , count);
   
    cout << count <<" Kaprekar numbers.";

	return 0 ; 
}
