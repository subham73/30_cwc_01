// Write a program to accept a 
// float value of number and return a rounded float value.


//status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


#define ps(x,y)	fixed<<setprecision(y)<<x


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
    float n ; 
	cout <<"Input a float number: ";
	cin >> n ;
	cout<<ps( n , 0)<<".00";


	return 0 ; 
}