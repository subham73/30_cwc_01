// Write a program to generate random 
// integers in a specific range.

// status : Done 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
	int min , max ; 
    cout<<"to enter the range put start and end value respectively :" ; 
    cin>>min >> max ;

    int n ;
    cout <<"enter the number of random values :";
    cin>> n ;
    for(int i = 0 ; i < n ; i++)
    {
    	cout << min + rand() %(max-min+1) <<" ";
    }

	return 0 ; 
}