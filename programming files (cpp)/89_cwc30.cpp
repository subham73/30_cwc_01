// Write a program to generate a magic 
//square of order n (all row, column, and diagonal sums are equal).


// status : done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

void magicSquare(int n)
{
    int square[n][n];

    for(int i = 0 ; i < n ; i++)
    {
    	for(int j =0 ; j < n ; j++)
    	{
    		square[i][j] = 0 ;
    	}
    }
 
    
    int i = n / 2;
    int j = n - 1;
 
    for (int num = 1; num <= n * n;) {
        if (i == -1 && j == n)
        {
            j = n - 2;
            i = 0;
        }
        else 
        {
            
            if (j == n)
                j = 0;

            if (i < 0)
                i = n - 1;
        }
        if (square[i][j]) 
        {
            j -= 2;
            i++;
            continue;
        }
        else
            square[i][j] = num++; 
        j++;
        i--; 
    }
 
    // op

    for (i = 0; i < n; i++) 
    {
        for (j = 0; j < n; j++)
            cout << square[i][j] << " ";
        cout << endl;
    }
}

int main()
{
	sj73();
    
	int n ; 
	cin>> n ;
    magicSquare(n);

	return 0 ; 
}

