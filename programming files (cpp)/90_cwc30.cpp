// Write a program to reverse an integer number.


// satus : solve 

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;


void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int reverse (int n )
{
	int num = 0 ; 
	while(n >0)
	{
		int rem = n % 10 ;
		num = num*10 + rem ;
		n /= 10 ;  
	}
	return num ; 
}

int main()
{
	sj73();
    
    int n ; 
    cin >> n ; 
    cout << "reverve number : "<< reverse( n ) ; 


	return 0 ; 
}