#include<bits/stdc++.h> 
using namespace std; 
using ll = long long;

void sj73()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    #ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

vector<int>fact,expo;                      //globally declared vectors
void Primefact(ll num ) 
{ 
        if(num ==1)                       // case for 1:
    { 
        fact.push_back(2); 
        expo.push_back(0); 
        return; 
    } 
    int d=2;                         //divisor 
    while(num>1 && 1LL *d*d<=num)    //LL stands for long long
    {    
        int count=0;  
        while(num%d==0)          // wont come out until a given d is long longer
        {                        // able to get divided .
                        count ++; 
            num/=d; 
        } 
        if(count>0) 
        { 
            fact.push_back(d); 
            expo.push_back(count); 
        } 
        d++; 
    } 
    if(num >1) 
    { 
        fact.push_back(num); 
        expo.push_back(1); 
    } 
} 
int main() 
{ 
    sj73();

    ll num  = 600851475143 ; 

    Primefact(num); 
    

    cout<<"The largest prime factor of the number 600851475143 is : "<<fact[fact.size() -1];
    return 0; 
}