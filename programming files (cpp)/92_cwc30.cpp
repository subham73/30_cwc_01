// Find the difference between the sum of the squares of 
// the first one hundred natural numbers and the square of the sum.


//Sttus : - Done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

// #define mod 1e9+7  //
// #define fin for(int i=0;i<n;i++)
// #define ff first
// #define ss second
// #define vi vector<int>
// #define vc vector<char>
// #define vii vector<int,int>
// #define mod 100000000/
// #define pb push_back
// #define mp make_pair
// #define ps(x,y)	fixed<<setpricision(y)<<x
// #define FASTIO ios_base::sync_with_stidio(false),cin.tie(NULL),cout.tie(NULL)

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
    
    int n = 100 ;
    ll s_f_sq = 0 , sq_f_s = 0 ; 

    for(int i = 1; i <= n ; i++)
    {
    	s_f_sq += pow(i ,2) ; 
    }

    sq_f_s = pow(n*(n+1) /2 ,2); 

    cout<<"difference between the sum of the squares of the first one
    		\n hundred natural numbers and the square of the sum "<< s_f_sq - sq_f_s ;


	return 0 ; 
}