// 2^15 = 32768 and the sum of its digits is 3 + 2 
//+ 7 + 6 + 8 = 26.What is the sum of the digits of the number 2^10



// status : Done
#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

#define MAX 100000
 
int multiply(int x, int res[], int res_size) 
{

	int carry = 0;
	 
	for (int i = 0; i < res_size; i++) 
	{
	    int prod = res[i] * x + carry;
	    res[i] = prod % 10;
	    carry = prod / 10;
	}
	while (carry) 
	{
	    res[res_size] = carry % 10;
	    carry = carry / 10;
	    res_size++;
	}

	return res_size;
}
 

ll power(int x, int n)
{
 	ll sum = 0 ; 

	if(n == 0 )
	{
	    cout<<"1";
	    return 0;
	}

	int res[MAX];
	int res_size = 0;
	int temp = x;
	 
	while (temp != 0) 
	{
	    res[res_size++] = temp % 10;
	    temp = temp / 10;
	}
	 

	for (int i = 2; i <= n; i++)
	    res_size = multiply(x, res, res_size);
	 
	
	for (int i = res_size - 1; i >= 0; i--)
    	sum += res[i] ;
    return sum ;  
}

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();
	ll sum = power(2, 1000);
	cout <<"sum : "<<sum ;
	return 0 ; 
}