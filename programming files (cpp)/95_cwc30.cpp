// The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17
// . Find the sum of all the primes below two million.


//status : Done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

const int Nmax = 2*1e6+1; 
bool isPrime[Nmax];

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

void Eratosthenes(int limit) 
{ 
    for(int i = 2 ; i <= limit ; i++) 
        isPrime[i] = true; 
    for(int i = 2 ; i <= limit/2 ; i++) 
    { 
        if(isPrime[i]==true) 
        { 
            for(int j = i*2 ; j <= limit ;j += i ) 
                isPrime[j] = false ; 
        } 
    }  
}

int main()
{
	sj73();
	int n = 2*1e6 ; 
    Eratosthenes(n) ; 
    ll sum = 0 ; 
    for(ll i = 2 ; i < n ; i++ )
    {
    	if(isPrime[i] == true )
    		sum+=isPrime[i] ; 
    }

    cout<<"The sum of prime below 2 million is :"<<sum ; 

	return 0 ; 
}