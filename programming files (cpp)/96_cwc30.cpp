// Find the last ten digits of the series,
// 1^1 + 2^2 + 3^3 + ... + 1000^1000.


//status : Done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int main()
{
	sj73();

	ll res = 0;
	ll MOD = 1e10;
	 
	for (int i = 1; i <= 1000; i++) 
	{
	    ll temp = i;
	    for (int j = 1; j < i; j++) 
	    {
	        temp *= i;
	        temp %= MOD;
	    }
	 
	    res += temp;
	    res %= MOD;

	}

	cout<<"Last 10 digits are : "<<res ;

	return 0 ; 
}