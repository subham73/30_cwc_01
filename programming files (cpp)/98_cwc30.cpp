// Find the sum of all numbers which 
// are equal to the sum of the factorial of their digits.

//status :Done



//array - > all emenets will be there 
// if there sum  = the number itself then sum +
// add it 


#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

#define MAX 500 

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

int bypass(int x, int res[], int res_size)
{
    int carry = 0;  
    for (int i=0; i<res_size; i++)
    {
        int prod = res[i] * x + carry; 
        res[i] = prod % 10; 
        carry  = prod/10;   
    }
    while (carry)
    {
        res[res_size] = carry%10;
        carry = carry/10;
        res_size++;
    }
    return res_size;
}

int fact_check (int n)
{
    int res[MAX];
 	int sum = 0 ;
    res[0] = 1;
    int res_size = 1;
 
    for (int x=2; x<=n; x++)
        res_size = bypass(x, res, res_size);
 

    for (int i=res_size-1; i>=0; i--)
        sum += res[i] ;
    if(sum == n)
    	return n ;
    return 0 ;
}
 



int main()
{
	sj73();
    int n ;
    ll result = 0  ; 
    cout<<"Enter the range :" ;
    cin >> n ; 
    for(int i = 1 ; i <= n ; i++ )
    {
		result += fact_check (i);
    }

    cout<<"sum of all numbers which are equal to the \n sum of the factorial of their digits :" <<result ;
    return 0;




}



//array - > all emenets will be there 
// if there sum  = the number itself then sum +
// add it 
