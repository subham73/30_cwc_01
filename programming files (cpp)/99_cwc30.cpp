//Program to calculate circular primes below one million?
// ex:- 73 and 37
//Status: Done

#include<bits/stdc++.h>
using namespace std;

using ll = long long;
using ld = long double;

const int Nmax = 1000001; 
bool isCirPrime[Nmax];

void sj73()
{
	ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
	#ifndef ONLINE_jUDGE
        freopen("input.txt","r", stdin);
        freopen("output.txt","w",stdout);
    #endif
}

void Eratosthenes(int limit) 
{ 
    for(int i = 2 ; i <= limit ; i++) 
        isCirPrime[i] = true; 
    for(int i = 2 ; i <= limit/2 ; i++) 
    { 
        if(isCirPrime[i]==true) 
        { 
            for(int j = i*2 ; j <= limit ;j += i ) 
                isCirPrime[j] = false ; 
        } 
    }  
}
int countDigits(int n)
{
    int digit = 0;
    while (n /= 10)
        digit++;
    return digit;
}
int Rotate(int n)
{
    int rem = n % 10;
    rem *= pow(10, countDigits(n));
    n /= 10; 
    n += rem; 
    return n;
}
void check(int limit )
{
	for(int i = 2 ; i <=limit ; i++)
	{
		if(isCirPrime[i] == true )
		{
			int rev = Rotate(i) ;
			if( isCirPrime[rev] == true)
				continue;
			else
			{
				isCirPrime[i] = false ;
				isCirPrime[rev] = false ; 
			}
		}
	}
}

int main()
{
	sj73();

	int n;
	cout<<"Enter the range : "; 
    cin>>n; 
    Eratosthenes(n); 
    check(n);
    for(int i = 2; i <= n ; i++) 
        if(isCirPrime[i] == true) 
            cout<< i << " "; 
    return 0;
 
}
